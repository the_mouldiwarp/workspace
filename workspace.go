package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"
)

// data structures and variables
// workdir data type
type workspace struct {
	date  string
	title string
	path  string
}

var basedir string

// gatArgs pulls in arguments provided with the command
func getArgs() []string {
	fmt.Println()
	if len(os.Args) < 2 { // 0 based index where 0 is the full command 1 onwards are arguments.
		log.Fatal("Please provide keywords for path; <work description>")
	}
	return os.Args[1:]
}

func buildSubPath(a []string) string {
	s := ""
	for _, v := range a {
		s += "_" + v
	}
	return s
}

// dateSting builds a string format of yyyymmdd
func dateString() string {
	t := time.Now()
	return fmt.Sprintf("%d%02d%02d",
		t.Year(), t.Month(), t.Day())
}

// returns true if directory / file exists, false if not
func createDirIfNotExist(p string) {
	if _, err := os.Stat(p); os.IsNotExist(err) {
		err = os.MkdirAll(p, 0755)
		if err != nil {
			log.Fatal("Could not create directory")
		}
	}
}

// getWorkdir reads an environmental variable and if missing uses hard coded
func getWorkDir() {
	wd := os.Getenv("WORKDIR")
	if wd != "" {
		basedir = wd
	} else {
		pwd, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
		}
		basedir = pwd
	}
}

// printStringArray outputs a string array to stdout
func printStringArray(a []string) {
	for _, i := range a {
		fmt.Println(i)
	}
}

func main() {
	getWorkDir()

	subPath := buildSubPath(getArgs())

	wd := workspace{dateString(), subPath, ""}
	wd.path = fmt.Sprintf("%s/%s%s", basedir, wd.date, wd.title)

	createDirIfNotExist(wd.path)
	fmt.Printf("cd %s", wd.path)
}
